﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Enemy : MonoBehaviour
{
    private readonly float maxColorDifference = 0.6f;
    private readonly Vector3 enemyBaseScale = new Vector3(0.2f, 0.2f, 1);
    private readonly (float, float) hRange = (0, 1);
    private readonly (float, float) sRange = (0.5f, 0.8f);
    private readonly float volume = 0.9f;
    private readonly float hueWeight = 1.2f;
    private readonly float appearanceTime = 0.2f;

    private float health;

    private void OnEnable()
    {
        Color randomColor = Color.HSVToRGB(Random.Range(hRange.Item1, hRange.Item2), Random.Range(sRange.Item1, sRange.Item2), volume);
        GetComponent<SpriteRenderer>().color = randomColor;
        health = 1;
        transform.localScale = Vector3.zero;
        transform.DOScale(enemyBaseScale, appearanceTime);
    }

    public void ReceiveDamage(Color playerColor)
    {
        Color32 enemyColor = GetComponent<SpriteRenderer>().color;

        float playerH, playerS, playerV, enemyH, enemyS, enemyV;

        Color.RGBToHSV(playerColor, out playerH, out playerS, out playerV);
        Color.RGBToHSV(enemyColor, out enemyH, out enemyS, out enemyV);

        float difference = Mathf.Round(10f * Mathf.Sqrt(Mathf.Pow(hueWeight * Mathf.Min(Mathf.Abs(playerH - enemyH), 1f - Mathf.Abs(playerH - enemyH)), 2) + Mathf.Pow(playerS - enemyS, 2) + Mathf.Pow(playerV - enemyV, 2))) / 10f;
        float damage = Mathf.Round(10f * 1f/(1 + Mathf.Exp(-40f * ((maxColorDifference - difference)/maxColorDifference - 0.7f)))) / 10f; // logistic function

        health -= damage;

        if (health <= 0)
            gameObject.SetActive(false);
        else
            transform.localScale -= new Vector3(damage / 10, damage / 10, 0);
    }

    private void OnDisable()
    {
        transform.DOKill();
    }
}
