﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIManager : MonoBehaviour
{
    public enum UIState {TitleScreen, PlayingScreen, PauseMenu, GameOver};

    public GameObject TitlePrefab;
    public Button PlayPrefab;
    public Text ScorePrefab;
    public Button PausePrefab;
    public Image DarkenerPrefab;
    public Button HelpPrefab;
    public Button SettingsPrefab;
    public GameObject EndScreenPrefab;

    private readonly float fadeTime = 0.5f;
    private readonly float darkenerOpacity = 0.66f;

    private GameObject canvas;
    private GameObject title;
    private Button playButton;
    private Text scoreText;
    private Button pauseButton;
    private Image darkener;
    private Button helpButton;
    private Button settingsButton;
    private GameObject endScreen;
    private Tweener blurTween;

    public void Init()
    {
        canvas = GameObject.Find("Canvas");

        title = Instantiate(TitlePrefab, canvas.transform);

        playButton = Instantiate(PlayPrefab, canvas.transform);
        playButton.onClick.AddListener(GetComponent<LevelManager>().StartGame);

        darkener = Instantiate(DarkenerPrefab, canvas.transform);

        scoreText = Instantiate(ScorePrefab, canvas.transform);

        pauseButton = Instantiate(PausePrefab, canvas.transform);
        pauseButton.onClick.AddListener(GetComponent<LevelManager>().SwitchPause);

        helpButton = Instantiate(HelpPrefab, canvas.transform);

        settingsButton = Instantiate(SettingsPrefab, canvas.transform);

        endScreen = Instantiate(EndScreenPrefab, canvas.transform);
        endScreen.transform.Find("Retry").GetComponent<Button>().onClick.AddListener(GetComponent<LevelManager>().StartGame);
    }

    public void SetScoreText(string score)
    {
        scoreText.text = score;
    }

    private void changeBackground(bool setBlurred)
    {
        var blur = Camera.main.GetComponent<SuperBlur.SuperBlur>();

        if (setBlurred)
        {
            var duration = fadeTime * (1 - blur.interpolation);

            blur.enabled = true;
            blurTween.Kill(false);
            blurTween = DOTween.To(() => blur.interpolation, x => blur.interpolation = x, 1, fadeTime * (1 - blur.interpolation)).SetUpdate(true);

            darkener.gameObject.SetActive(true);
            darkener.DOKill(false);
            darkener.DOFade(darkenerOpacity, duration).SetUpdate(true);
        }
        else
        {
            var duration = fadeTime * blur.interpolation / 2;

            blurTween.Kill(false);
            blurTween = DOTween.To(() => blur.interpolation, x => blur.interpolation = x, 0.1f, duration).SetUpdate(true).OnComplete(() => blur.enabled = false);

            darkener.DOKill(false);
            darkener.DOFade(0, duration).SetUpdate(true).OnComplete(() => darkener.gameObject.SetActive(false));
        }
    }

    public void OpenPauseMenu()
    {
        changeBackground(true);

        helpButton.gameObject.SetActive(true);

        /*
        helpButton.gameObject.SetActive(true);
        helpButton.GetComponentInChildren<Text>().DOKill(false);
        helpButton.GetComponentInChildren<Text>().DOFade(1, duration).SetUpdate(true);

        settingsButton.gameObject.SetActive(true);
        settingsButton.GetComponentInChildren<Text>().DOKill(false);
        settingsButton.GetComponentInChildren<Text>().DOFade(1, duration).SetUpdate(true);
        */
    }

    public void ClosePauseMenu()
    {
        var blur = Camera.main.GetComponent<SuperBlur.SuperBlur>();
        
        var duration = fadeTime * blur.interpolation / 2;

        blurTween.Kill(false);
        blurTween = DOTween.To(() => blur.interpolation, x => blur.interpolation = x, 0.1f, duration).SetUpdate(true).OnComplete(() => blur.enabled = false);

        darkener.DOKill(false);
        darkener.DOFade(0, duration).SetUpdate(true).OnComplete(() => darkener.gameObject.SetActive(false));

        helpButton.GetComponentInChildren<Text>().DOKill(false);
        helpButton.GetComponentInChildren<Text>().DOFade(0, duration).SetUpdate(true).OnComplete(() => helpButton.gameObject.SetActive(false));

        settingsButton.GetComponentInChildren<Text>().DOKill(false);
        settingsButton.GetComponentInChildren<Text>().DOFade(0, duration).SetUpdate(true).OnComplete(() => helpButton.gameObject.SetActive(false));
    }

    public void SwitchUI (UIState state)
    {
        switch(state)
        {
            case UIState.PlayingScreen:
                changeBackground(false);

                title.SetActive(false);
                playButton.gameObject.SetActive(false);

                helpButton.gameObject.SetActive(false);
                settingsButton.gameObject.SetActive(false);

                endScreen.SetActive(false);

                scoreText.gameObject.SetActive(true);
                pauseButton.gameObject.SetActive(true);
                break;
            case UIState.PauseMenu:
                changeBackground(true);

                helpButton.gameObject.SetActive(true);
                settingsButton.gameObject.SetActive(true);
                break;
            case UIState.GameOver:
                changeBackground(true);

                pauseButton.gameObject.SetActive(false);

                endScreen.SetActive(true);
                break;
        }
    }
}
