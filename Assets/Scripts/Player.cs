﻿using UnityEngine;

public class Player : MonoBehaviour
{
    private readonly (float, float) hRange = (0f, 1f);
    private readonly (float, float) sRange = (0.5f, 0.8f);
    private readonly float volume = 0.9f;
    private readonly int swipeRangeHue = 6;
    private readonly int swipeRangeSaturation = 2;

    private Vector2 touchOrigin;
    private bool isGameOn = false;

    void Update()
    {
        if (!isGameOn)
            return;

        if (Input.touchCount > 0)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began)
                OnTouchBegan();
            else if (Input.GetTouch(0).phase == TouchPhase.Moved)
                OnTouchMoved();
            else if (Input.GetTouch(0).phase == TouchPhase.Ended)
                OnTouchEnded();
        }
    }

    private void OnEnable()
    {
        Color randomColor = Color.HSVToRGB(Random.Range(hRange.Item1, hRange.Item2), Random.Range(sRange.Item1, sRange.Item2), volume);
        GetComponent<SpriteRenderer>().color = randomColor;
    }

    private void OnTouchBegan()
    {
        touchOrigin = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
    }

    private void OnTouchEnded()
    {
        var touchEnd = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);

        RaycastHit2D hitInformation = Physics2D.Raycast(touchEnd, Camera.main.transform.forward);

        if (hitInformation.collider != null)
        {
            if (hitInformation.collider.CompareTag("Player"))
            {
                var playerColor = GetComponent<SpriteRenderer>().color;
                GameManager.Instance.gameObject.GetComponent<LevelManager>().DealDamage(playerColor);
            }
        }
    }

    private void OnTouchMoved()
    {
        float oldHue, oldSaturation, newHue, newSaturation;

        Color.RGBToHSV(GetComponent<SpriteRenderer>().color, out oldHue, out oldSaturation, out float temp);

        Vector2 touchPosition = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
        Vector2 delta = touchPosition - touchOrigin;

        newSaturation = oldSaturation + delta.x / swipeRangeSaturation;

        if (newSaturation < sRange.Item1)
            newSaturation = sRange.Item1;
        else if (newSaturation > sRange.Item2)
            newSaturation = sRange.Item2;

        newHue = oldHue + delta.y / swipeRangeHue;

        if (newHue < hRange.Item1)
            newHue = hRange.Item2 - (hRange.Item1 - newHue);
        else if (newHue > hRange.Item2)
            newHue = newHue - hRange.Item1;

        GetComponent<SpriteRenderer>().color = Color.HSVToRGB(newHue, newSaturation, volume);

        touchOrigin = touchPosition;
    }

    public void SetGameState(bool state)
    {
        isGameOn = state;
    }
}
