﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    [System.Serializable]
    public class PooledObjectType
    {
        public int AmountToPool;
        public GameObject ObjectToPool;
        public string Name;
    }

    public List<PooledObjectType> PooledObjectTypes;
    
    public Dictionary<string, List<GameObject>> PooledObjects { get; private set; }

    public void InitPool()
    {
        PooledObjects = new Dictionary<string, List<GameObject>>();

        List<GameObject> pool;
        GameObject parentGroup;

        foreach (var type in PooledObjectTypes)
        {
            pool = new List<GameObject>();
            PooledObjects.Add(type.Name, pool);

            parentGroup = new GameObject(type.Name);

            for (int i = 0; i < type.AmountToPool; i++)
            {
                AddToPool(parentGroup, type.Name);
            }
        }
    }

    private GameObject AddToPool(GameObject parentGroup, string groupName)
    {
        GameObject pooledObject;
        GameObject objectPrefab = (PooledObjectTypes.Where(type => type.Name == groupName).First()).ObjectToPool;

        pooledObject = (GameObject)Instantiate(objectPrefab, parentGroup.transform);
        pooledObject.gameObject.SetActive(false);
        PooledObjects[groupName].Add(pooledObject.gameObject);

        return pooledObject;
    }

    public GameObject GetPooledObject(string groupName)
    {
        GameObject pooledObject = null;

        for (int i = 0; i < PooledObjects[groupName].Count; i++)
        {
            if (!PooledObjects[groupName][i].activeInHierarchy)
            {
                pooledObject = PooledObjects[groupName][i];
                break;
            }
        }

        if (pooledObject == null)
        {
            GameObject parentGroup = GameObject.Find(groupName);
            pooledObject = AddToPool(parentGroup, groupName);
        }

        return pooledObject;
    }

    public List<GameObject> GetActiveObjects(string groupName)
    {
        List<GameObject> activeObjects = new List<GameObject>();

        foreach (var obj in PooledObjects[groupName])
            if (obj.activeInHierarchy)
                activeObjects.Add(obj);

        return activeObjects;
    }
}
