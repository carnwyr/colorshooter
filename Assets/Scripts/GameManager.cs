﻿using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance = null;

    private UIManager uiScript;
    private LevelManager levelScript;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);

        uiScript = GetComponent<UIManager>();
        levelScript = GetComponent<LevelManager>();

        InitGame();
    }

    void InitGame()
    {
        uiScript.Init();
        levelScript.InitGame();
        //levelScript.StartGame();
    }
    
    void Update()
    {
        
    }
}
