﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class LevelManager : MonoBehaviour
{
    public GameObject playerPrefab;

    private readonly float[] originX = { -1.9f, -1.52f };
    private readonly float originY = 4f;
    private readonly float stepX = 0.76f;
    private readonly float stepY = 0.8f;
    private readonly int rowWidth = 5;
    private readonly float repeatingTime = .5f;
    
    private int rowCounter = 0;
    private int score = 0;
    private bool isOver = false;
    private bool isPaused = false;

    private ObjectPooler poolerScript;
    private UIManager uiScript;
    private Player player;

    public void InitGame()
    {
        player = Instantiate(playerPrefab).GetComponent<Player>();
        player.gameObject.SetActive(false);

        poolerScript = GetComponent<ObjectPooler>();
        poolerScript.InitPool();

        uiScript = GetComponent<UIManager>();
    }

    public void StartGame()
    {
        uiScript.SwitchUI(UIManager.UIState.PlayingScreen);

        score = 0;
        uiScript.SetScoreText("0");

        foreach (var existingEnemy in poolerScript.GetActiveObjects("Enemies"))
        {
            existingEnemy.SetActive(false);
        }

        InvokeRepeating("AddEnemyRow", 0f, repeatingTime);

        isOver = false;

        player.gameObject.SetActive(true);
        player.SetGameState(true);
    }

    private void AddEnemyRow()
    {
        GameObject enemy;

        for (int i = 0; i < rowWidth + (1 - rowCounter % 2); i++)
        {
            enemy = poolerScript.GetPooledObject("Enemies");
            enemy.transform.position = new Vector3(originX[rowCounter % 2] + i * stepX, originY, 0f);
            enemy.SetActive(true);
            enemy.GetComponent<Rigidbody2D>().velocity = new Vector3(0, -stepY / repeatingTime, 0);
        }

        rowCounter = 1 - rowCounter;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!isOver && collision.CompareTag("Enemy"))
            EndGame();
    }

    public void DealDamage(Color playerColor)
    {
        foreach (var existingEnemy in poolerScript.GetActiveObjects("Enemies"))
        {
            existingEnemy.GetComponent<Enemy>().ReceiveDamage(playerColor);
            if (!existingEnemy.activeSelf)
            {
                score++;
                uiScript.SetScoreText(score.ToString());
            }
        }
    }

    private void EndGame()
    {
        isOver = true;

        CancelInvoke("AddEnemyRow");

        foreach (var existingEnemy in poolerScript.GetActiveObjects("Enemies"))
            existingEnemy.GetComponent<Rigidbody2D>().velocity = new Vector3(0, 0, 0);

        player.SetGameState(false);

        uiScript.SwitchUI(UIManager.UIState.GameOver);
    }

    public void SwitchPause()
    {
        if (!isPaused)
        {
            isPaused = true;
            Time.timeScale = 0f;
            
            player.SetGameState(false);

            uiScript.SwitchUI(UIManager.UIState.PauseMenu);
        }
        else
        {
            uiScript.SwitchUI(UIManager.UIState.PlayingScreen);

            player.SetGameState(true);

            isPaused = false;
            Time.timeScale = 1.0f;
        }
    }
}
