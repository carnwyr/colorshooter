﻿using UnityEngine;

public class Loader : MonoBehaviour
{
    public GameObject localizationManager;
    public GameObject gameManager;

    void Awake()
    {
        if (LocalizationManager.Instance == null)
            Instantiate(localizationManager);

        if (GameManager.Instance == null)
            Instantiate(gameManager);
    }
}
